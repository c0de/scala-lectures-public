<!DOCTYPE html>
<html>

<head>
    <title>Lecture Futures </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="lectures-style.css">
    <link rel="icon" href="favicon.ico" />
</head>

<body>
    <textarea id="source">

class: center, middle

<!-- page_number: true -->
<!-- footer: Cours Scala - 2020 - by Joris Beau-->

# Introduction to
![](./images/scala_logo.png)

## 2020 - Lecture - Futures


---

# Futures

- parallel execution
- asynchronous
- non-blocking

=>

- composition of concurrent events
- simplify reasoning about concurrent operations

https://docs.scala-lang.org/overviews/core/futures.html

---

# Future: placeholder

```scala
trait Future[+T] extends Awaitable[T]
```

A Future is an object containing a value that may not exist yet:
- the computation has completed:
    - with a value: `Future.successful(value)`
    - with an exception: `Future.failed(exception)`

- the computation has not completed yet

---

# Create Future

```scala
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global // discussed later

// Completed: successful
Future.successful(1)

// Completed: failed
Future.failed(new Exception("failed!!!"))

// To be computed
val f: Future[List[User]] = Future {
  db.getUsers()  
}
```

???

What's wrong with f? pagination

---

# Callbacks

Provide non-blocking operators

Use _callbacks_ to access the value of the Future once it is available
  - `def onComplete[U](f: (Try[T]) => U): Unit`
  - `def foreach[U](f: (T) => U): Unit`
  - `def andThen[U](pf: PartialFunction[Try[T], U]): Future[T]`

---

# Callbacks

"We say that the callback is executed eventually."*

- executed after the completion of the future (obviously)
- callbacks on a same future are not ordered (may change from different run)
- no necessarily executed by the same thread
- all callbacks are executed, regardless of the result of the others

*_eventually translates to "finalement" in french_

---

# Callbacks: `onComplete`

`def onComplete[U](f: (Try[T]) => U): Unit`

- applies the provided function `f` upon completion

```scala
val future: Future[Int] = ???

future.onComplete({
  case Success(value) => println(value) // or another side-effect
  case Failure(exception) => println(exception.getMessage)
})
```

---

# Callbacks: `foreach`

`def foreach[U](f: (T) => U): Unit`

- applies the provided function `f` upon completion
- only handles successful case

```scala
val future: Future[Int] = ???

future.foreach(value => println(value))

// or just
future.foreach(println)
```

---

# Callbacks: `andThen`

`def andThen[U](pf: PartialFunction[Try[T], U]): Future[T]`

- applies the provided function `f` upon completion

```scala
val future: Future[Int] = Future.successful(1)

future
  .andThen{ case Success(x) if x >= 0) => println(x)}
  .andThen{ case Success(x) if x < 0 => println(s"-$x")}
```

---

# Transformers

Use _transformers_
  - `def map[S](f: (T) ⇒ S)(implicit executor: ExecutionContext): Future[S]`
  - `def flatMap[S](f: (T) ⇒ Future[S])(implicit executor: ExecutionContext): Future[S]`
  - `def recover[U >: T](pf: PartialFunction[Throwable, U]): Future[U]`
  - `def fallbackTo[U >: T](that: Future[U]): Future[U]`

With such methods, the execution is deterministic (given than Futures completes with the same results).

---

# Transformers `map`

`def map[S](f: (T) ⇒ S)(implicit executor: ExecutionContext): Future[S]`
"Creates a new future by applying a function to the successful result of this future."

```scala
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

val f: Future[Int] = Future { 1 } // value: Future(Success(1))

f
  .map(_ + 1)
  .onComplete {
      case Success(value) => println(value) // prints 2
      case Failure(error) => println(error)
  }
```

---

# Transformers `flatMap`

`def flatMap[S](f: (T) ⇒ Future[S])(implicit executor: ExecutionContext): Future[S]`
"Creates a new future by applying a function to the successful result of this future, and returns the result of the function as the new future."

```scala
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

val nbUsersA: Future[Int] = Future.successful(1)
val nbUsersB: Future[Int] = Future.successful(2)

// Future[Future[Int]] :(
val result1: Future[Future[Int]] = nbUsersA.map { nbA =>
  val temp: Future[Int] = nbUsersB.map (nbB => nbA + nbB)
  temp
}

// Future[Int] :)
val result2: Future[Int] = nbUsersA.flatMap { nbA =>
  val temp: Future[Int] = nbUsersB.map(nbB => nbA + nbB)
  temp
}
```

---

# Transformers `recover`

`def recover[U >: T](pf: PartialFunction[Throwable, U]): Future[U]`
"Creates a new future that will handle any matching throwable that this future might contain."

```scala
Future(6 / 0).recover { case e: ArithmeticException => 0 }
```

- `recoverWith` does the same with a future in parameter

---

# Transformers `fallbackTo`

- `def fallbackTo[U >: T](that: Future[U]): Future[U]`
"Creates a new future which holds the result of this future if it was completed successfully, or, if not, the result of the that future if that is completed successfully."

```scala
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

val precise: Future[Int] = computePrecisely()
val approximate: Future[Int] = computeAnApproximateValue()

val result = precise fallbackTo approximate
```

---

# Example

```scala
val f1 = Future.successful(1)
val f2 = Future.successful(2)
val f3 = Future.successful(3)

// Future[Int]
f1.flatMap(v1 =>
  val v4 = 4
  f2.flatMap(v2 =>
    f3.map(v3 =>
      v1 + v2 + v3 + v4
    )
  )
)
```

---

# For-comprehension (in brief)

Futures support `for-comprehension` (not a loop!)

```scala
val f1 = Future.successful(1)
val f2 = Future.successful(2)
val f3 = Future.successful(3)

for {
    v1 <- f1
    v4 = 4
    v2 <- f2
    v3 <- f3
} yield v1 + v2 + v3 + v4
```

.note[It works also for other placeholder types]

---

# Future's companion object

### sequence
```scala
val f: Future[List[Int]] = Future.sequence(
  List(f1, f2, f3)
)
```

---

# Future's companion object

### fromTry
```scala
for {
  user <- db.findUserById("toto")
  f <- Future.fromTry(
    Try(heavyThrowableJavaMethod(user.name))
  )
} yield f
```

---

# Blocking

```scala
import scala.concurrent.Await
import scala.concurrent.duration._

val f: Future[Int] = ...

Await.result(f, 10.seconds) // blocks for 10s maximum
```

---

# Promises

- Future are "read-only" placeholder object for a a value that does not exist yet
- Promises are a "single write" object which completes to a `Future`

```scala
import scala.concurrent.Promise

val p = Promise[Int]()

val f: Future[Int] = if (scala.math.random() > 0,5) {
  p.success(1)
  // p.success(1) completing a second time will fail with IllegalStateException
} else {
  p.failure(0)
}
```

Usage example: multiple HTTP requests coming to the server, reply only once

---

# ExecutionContext

- Futures and Promises need an `ExecutionContext` for executing the computations
- `ExecutionContext` hides a Java's `Executor` handling a thread or pool of thread
  - Using the current thread is discouraged
- Scala's provide a static global `ExecutionContext`
  - `import scala.concurrent.ExecutionContext.global`

---

# Curious example

```scala
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

def sideEffect(x: Int)(implicit ec: ExecutionContext) = Future{
  println(s"sideEffect $x")
  Thread.sleep(1000)
  x
}

import scala.concurrent.ExecutionContext.Implicits.global
val l = List(
  (sideEffect(1), true),
  (sideEffect(2), false),
  (sideEffect(3), true)
)

l.forall{ case (a, b) =>
  a.onComplete({
    case Failure(exception) => println(exception.getMessage)
    case Success(value) => println(value)
  })
  b
}
```

---

# Curious example (cont'd)

### Compare with

```scala
def sideEffect(x: Int) = println(x)

val l = List(
  (sideEffect(1), true),
  (sideEffect(2), false),
  (sideEffect(3), true)
)

l.forall{ case (a, b) =>
  b
}
```

.question[What does it say about Futures? What can we conclude from this?]

---

# Curious example: comment

- Futures are _eager_
  - the future is executed immediately at its creation
  - code less easy to read/ write
  - reasoning more difficult
- Futures are not stoppable

???

TODO: Error channel is only Throwable

"Failure" vs "Errors"

---

# End

    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script>
        remark.macros.img = function(altText, width) {
            var url = this;
            return '<img alt="' + altText + '" src="' + url + '" style="width: ' + width + '" />';
        };

        var slideshow = remark.create();
    </script>
</body>

</html>