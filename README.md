This repository hosts the webpage for Scala lectures

# Webpage
- the webpage is hosted by Gitlab
- the Gitlab CICD automatically delivers the new content, see `.gitlab-ci.yml`
- the content comes from the directory `/public`
- the content consists of `index.html` and `index-style.css`

# Lectures slides
- _remarkjs_ combines HTML and markdown
  - the slides are written in markdown
  - the slides are wrapped in a HTML files
- the slides can simply be open with a browser
- the slides style can be changed in `lectures-style.css`, ex:
  - `.question[...]` -> for questions to the audiance
  - `.note[...]` -> for important informations
  - `.credit[...]`  for "bibliography" or links
